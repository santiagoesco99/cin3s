DESCRIPCION DEL PROYECTO

El proyecto que nosotros realizaremos será ‘CIN3S’, el cuál consiste en la página web de la reserva de las diferentes películas que se ofrecerán en dicha página. Para cada película se tendrá en cuenta la sala, el tipo de proyección, el doblaje de la película, los horarios y las sillas que se escogen, la variación de estas afectarán en el precio final de la reserva, la comida ya será por aparte de la reserva, contará con días de promoción, es decir, depende del dia en que se reserve la función, puede costar más o menos. La página contará con diferentes métodos de pago para facilitar la compra de este.

TECNOLOGÍAS

Express
Node
React

INTEGRANTES

Sebastián Jair Murillo
Santiago Posada
Santiago Escobar
